## Adding Event

Simple Adding Event project on Laravel with Vue.js.

- Clone the repository with git clone
- Copy .env.example file to .env and edit database credentials there
- Run composer install
- Run php artisan key:generate
- Run php artisan migrate
- Run npm install
- Run npm run dev

That's it - load the homepage