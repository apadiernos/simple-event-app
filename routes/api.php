<?php

// api.php

use App\Http\Controllers\EventController;
use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/event/create',  [EventController::class, 'store']);