<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Add Event Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during adding events for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'date_from' => 'Date From',
    'date_to' => 'Date To',
    'event' => 'Event',
    'Monday' => 'Monday',
    'Tuesday' => 'Tuesday',
    'Wednesday' => 'Wednesday',
    'Thursday' => 'Thursday',
    'Friday' => 'Friday',
    'Saturday' => 'Saturday',
    'Sunday' => 'Sunday'

];
