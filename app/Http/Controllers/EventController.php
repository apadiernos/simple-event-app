<?php

namespace App\Http\Controllers;

// EventController.php

use App\Event;
use App\EventDate;
use App\Http\Resources\EventCollection;
use DayHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class EventController extends Controller
{
    
    /**
     * Store the event data and event dates.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return object|exception
     */
    public function store(Request $request)
    {
        

        $this->validate($request, [
            'event' => 'required|max:191',
            'date_from' => 'required|regex:/\d{1,2}\/\d{1,2}\/\d{4}/',
            'date_to' => 'required|regex:/\d{1,2}\/\d{1,2}\/\d{4}/',
            'days' => 'required|array'
        ], [
            'event.required' => __('validation.required', ['attribute' => 'Event']),
            'event.max' => __('validation.max.numeric', ['attribute' => 'Event']),
            
            'date_from.required' => __('validation.required', ['attribute' => 'Date From']),
            'date_from.regex' => __('validation.date_format', ['attribute' => 'Date From']),
            'date_to.required' => __('validation.required', ['attribute' => 'Date To']),
            'date_to.regex' => __('validation.date_format', ['attribute' => 'Date To']),            

            'days.array' => __('validation.required', ['attribute' => 'Days']),
            'days.required' => __('validation.required', ['attribute' => 'Days']),
        ]);

        $event = new Event([
            'event' => $request->get('event'),
        ]);

        $event->save();
        $date_from = str_replace('/', '-',$request->get('date_from'));
        $date_to = str_replace('/', '-',$request->get('date_to'));

        $days_array = DayHelper::GetYeardays($date_from,$date_to,$request->get('days'),$request->get('event'));
       foreach( $days_array as $dayNumber => $selectedDay  ){
            $event->eventdates()->save(new EventDate(["event_day_name" => $selectedDay['day_numeric'] ,"event_date" => $selectedDay['date_unix']]));
        }
 
        return response()->json($days_array);
    }

}