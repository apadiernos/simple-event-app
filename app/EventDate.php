<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventDate extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_id','event_day_name','event_date'
    ];
}
