<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event'
    ];

 	public function eventdates() {
		$record = $this->hasMany('App\EventDate','event_id','id');
		return $record;
	}
   
}
