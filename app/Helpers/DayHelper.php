<?php

namespace App\Helpers;

class DayHelper {

    /*
    |--------------------------------------------------------------------------
    | Default Daynames
    |--------------------------------------------------------------------------
    |
    | These are the equivalent of values of Day Names into numeric
    |
    */

    const DEFAULT_DAYNAMES  = [
      'Sun' => 1,
      'Mon' => 2,
      'Tue' => 3,
      'Wed' => 4,
      'Thu' => 5,
      'Fri' => 6,
      'Sat' => 7
    ];

    /**
     * Get all the dates in the date range based on the selected name of day
     * We will be preparing our event objects that will be used in fullcalendar
     *
     * @param  date $dateStart - start date
     * @param  date $dateend - end date
     * @param  array $dayNames - selected day names
     * @param  string $event - event name
     * @return array
     */
   public static  function GetYeardays($dateStart, $dateend, $dayNames, $event){
        $period = new \DatePeriod(
            new \DateTime($dateStart), new \DateInterval('P1D'), (new \DateTime($dateend))
        );
        $dates = iterator_to_array($period);

        $arrayreturn = array();  
        $i = 0;  
        foreach ($dates as $val) {
            $date = $val->format('Y-m-d'); //format date
            $get_name = date('l', strtotime($date)); //get week day
            $day_trim_name = substr($get_name, 0, 3); // Trim day name to 3 chars
            $day_numeric = self::DEFAULT_DAYNAMES[$day_trim_name];
            if( in_array($day_numeric, $dayNames) ){
                 $arrayreturn[] = [
                    'id' => $i++,
                    'title' => $event,
                    'start' => date("Y-m-d",strtotime($date)),
                    'end' => date("Y-m-d",strtotime($date)),
                    'date_unix' => strtotime($date),
                    'allDay' => true,
                    'day_numeric' => $day_numeric,
                 ];
            }     
        }
        return $arrayreturn;
    }
} 